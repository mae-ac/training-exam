def test_car():
    from src.car import Car

    car = Car("Toyota", "Corolla", 2015)
    assert car.get_description() == "2015 Toyota Corolla"
