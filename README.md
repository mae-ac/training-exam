# Training Exam

The purpose of this project is to check if everything works correctly.

:warning: **WARNING**
Obviously, the real exam will be longer and harder than this one.

If you have a problem, mail me : jonathan.detchart@isae-supaero.fr

## Preparing your environment on the ISAE's computers 

*The exam must be done on Linux, not on Windows*

Run the following steps:
- open a `Terminal` on the computer
- clone this repository using the following command:
```bash
git clone https://gitlab.isae-supaero.fr/mae-ac/training-exam.git
```
- Load the `Anaconda` module using the following command:
```bash
module load anaconda3/2024
```
- Activate the `Python` environment using the following command:
```bash
source activate mae1-env
```
- Go inside the exam folder:
```bash
cd training-exam
```
- Run your editor or you IDE. For vscode, run:
```bash
code .
```

## Questions

### Q1

In `car.py`, create a class `Car` that represents a car with attributes like `brand`, `model`, and `year`.
Implement a method `get_description` that describes the car.
Implement a method `update_year` that allows you to update the car's year.

### Q2

Add a method get_description to the class that returns a formatted string "`year` `brand` `model`", describing the car (e.g., "2020 Toyota Corolla").

### Q3

Add another method update_year to update the year of the car.

### Q4 

- In the same file, create 2 instances of a `Car` : a `Toyota` `Corolla` from `2020`, and a `Tesla` `Model S` from `2021`
- Print the description of both `Car`
- Update the `year` of the first one by setting to `2022`
- Print the description of the modified `Car`

## Upload your work

- open a terminal
- go into the folder containing your project
- use the `zip` command to compress your project
```shell
zip -r project.zip . -x "venv/**" ".git/**"
```
- upload the ZIP archive to the [LMS] (you will find a link here for the real exam)
